;;; Packages
(setq package-archives '(("org" . "http://orgmode.org/elpa/")
                         ("gnu" . "http://elpa.gnu.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))
(require 'package)
(package-initialize)
(defvar my-packages '(;; Core
                      evil
                      evil-leader
                      evil-ediff
                      helm
                      helm-backup
                      helm-gtags
                      telephone-line
                      diminish
                      company
                      company-irony
                      ;;helm-company
                      clang-format
                      exec-path-from-shell
                      irony
                      ;;expand-region
                      ;;ace-jump-mode
                      flycheck
                      flycheck-irony
                      magit
                      projectile
                      sudo-edit
		      cmake-ide
		      rtags
                      cedet
		      ; markdown-mode
                      ;json-mode
                      ;less-css-mode
                      ;web-mode js2-mode
                      rainbow-delimiters
                      highlight
                      paredit
                      evil-paredit
                      aggressive-indent ; Lisp
                      ))
(defun my-missing-packages ()
  (let (missing-packages)
    (dolist (package my-packages missing-packages)
      (or (package-installed-p package)
          (push package missing-packages)))))

(let ((missing (my-missing-packages)))
  (when missing
    ;; Check for new packages (package versions)
    (package-refresh-contents)
    ;; Install the missing packages
    (mapc (lambda (package)
            (when (not (package-installed-p package))
              (package-install package))) missing)
    ;; CLOSE the compilation log.
    (let ((compile-window (get-buffer-window "*Compile-Log*")))
      (if compile-window
          (delete-window compile-window)))))


;;================================= Bars ========================
(tool-bar-mode -1)
(menu-bar-mode -1)

;;================================= Evil ========================
(require 'evil)
(require 'evil-leader)

(setq evil-leader/in-all-states 1)
(global-evil-leader-mode)
(evil-leader/set-leader ",")
(evil-mode 1)
(setq evil-shift-width 2)

(evil-leader/set-key
  "e" 'helm-find-files
  "l" 'helm-locate
  "b" 'helm-buffers-list
  "w" 'save-buffer
  "k" 'kill-current-buffer
  "o" 'helm-backup
  "x" 'helm-M-x)

;;================================= Helm ========================
(require 'helm)
(require 'helm-config)

(recentf-mode 1)
(setq helm-ff-file-name-history-use-recentf t)

(helm-mode 1)
(global-eldoc-mode 0)

;;================================= Helm-backup =================
(require 'helm-backup)
(add-hook 'after-save-hook 'helm-backup-versioning)
(setq make-backup-files nil)



;;================================= Complete and correct ========
(require 'company)
(global-company-mode t)
(setq company-dabbrev-downcase nil)
;;================================= General =====================
(setq inhibit-splash-screen t)
(load-theme 'wombat t)

;(global-hl-line-mode 1)
;(set-face-attribute hl-line-face nil :underline nil)
(global-linum-mode 1)

;;================================= Projectile====================
(projectile-mode)


;;================================= Modeline =====================
(require 'telephone-line-config)
(telephone-line-evil-config)

(setq telephone-line-primary-left-separator 'telephone-line-cubed-left
      telephone-line-secondary-left-separator 'telephone-line-cubed-hollow-left
      telephone-line-primary-right-separator 'telephone-line-cubed-right
      telephone-line-secondary-right-separator 'telephone-line-cubed-hollow-right)
(setq telephone-line-height 24
      telephone-line-evil-use-short-tag t)
(setq telephone-line-lhs
        '((evil   . (telephone-line-evil-tag-segment))
          (accent . (telephone-line-vc-segment
                     telephone-line-erc-modified-channels-segment
                     telephone-line-process-segment))
          (nil    . (telephone-line-minor-mode-segment
                     telephone-line-buffer-segment))))
(setq telephone-line-rhs
        '((nil    . (telephone-line-misc-info-segment))
          (accent . (telephone-line-major-mode-segment))
          (evil   . (telephone-line-airline-position-segment))))

(telephone-line-mode t)

(require 'diminish)
(diminish 'company-mode)
(diminish 'helm-mode)
(diminish 'undo-tree-mode)
(diminish 'flycheck-mode)

(diminish 'abbrev-mode)
(eval-after-load 'auto-revert-mode
  '(diminish 'auto-revert-mode))
(eval-after-load 'flycheck
  '(diminish 'flycheck-mode))

;;================================= Tabs =====================
(setq-default indent-tabs-mode nil)
(setq-default default-tab-width 2)

;;================================= clang-format =====================
(require 'clang-format)

;;================================= flycheck =====================
(require 'flycheck)
;; flycheck-mode
(global-flycheck-mode)

(setq-default flycheck-disabled-checker '(emacs-lisp-checkdoc))



(global-flycheck-mode)

;;================================= Pair delimiters ===========
(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(require 'cl-lib)
(require 'color)
(cl-loop
 for index from 1 to rainbow-delimiters-max-face-count
 do
 (let ((face (intern (format "rainbow-delimiters-depth-%d-face" index))))
   (cl-callf color-saturate-name (face-foreground face) 50)))
(set-face-attribute 'rainbow-delimiters-unmatched-face nil
                    :foreground 'unspecified
                    :inherit 'error
                    :strike-through t)

;;============================== C++ ========================
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

(defun clang-save ()
  "Run clang-format and save."
  (interactive)
  (clang-format-buffer)
  (save-buffer)
)



(require 'helm-gtags)
(setq
 helm-gtags-ignore-case t
 helm-gtags-auto-update t
 helm-gtags-use-input-at-cursor t
 helm-gtags-pulse-at-cursor t
 helm-gtags-prefix-key "\C-cg"
 helm-gtags-suggested-key-mapping t
 )

(add-hook 'dired-mode-hook 'helm-gtags-mode)
(add-hook 'eshell-mode-hook 'helm-gtags-mode)
(add-hook 'c-mode-hook 'helm-gtags-mode)
(add-hook 'c++-mode-hook 'helm-gtags-mode)
(add-hook 'asm-mode-hook 'helm-gtags-mode)

(define-key helm-gtags-mode-map (kbd "C-c g a") 'helm-gtags-tags-in-this-function)
(define-key helm-gtags-mode-map (kbd "C-j") 'helm-gtags-select)
(define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
(define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
(define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
(define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history)

(evil-leader/set-key-for-mode 'c++-mode
  "w" 'clang-save
  "g" 'helm-gtags-dwim)

;;============================== Startup ========================
;;(helm-recentf)


 

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default)))
 '(package-selected-packages
   (quote
    (solarized-theme color-theme-solarized rony clang-format evil-leader evi-ediff helm-backup telephone-line company magit sudo-edit))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
