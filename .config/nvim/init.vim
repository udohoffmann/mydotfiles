set nocompatible
"" Skip intro
set shortmess=atI
""Window bar filename
set title
set scrolloff=3

set tabstop=2
set expandtab

set mouse=va

set encoding=UTF-8
set noswapfile

let mapleader = "\<Space>"

syntax on
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

command! -bang -nargs=* Find
  \ call fzf#vim#grep(
  \   'rg -w --column --line-number --no-heading --color=always '.shellescape(expand('<cword>')), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)

noremap <C-f> :Files<CR>
noremap <C-g> :Rg<CR>
noremap <C-l> :Locate 
noremap <C-g> :Find<CR>

set background=light

call plug#begin()
Plug 'itchyny/lightline.vim'
" Show yanked stuff
Plug 'machakann/vim-highlightedyank'
" Extend % functionality
Plug 'andymass/vim-matchup'
" Open in project root
Plug 'airblade/vim-rooter'
Plug 'mhinz/vim-startify'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" Semantic language support
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Syntactic language support
Plug 'cespare/vim-toml'
Plug 'stephpy/vim-yaml'
Plug 'rust-lang/rust.vim'
Plug 'rhysd/vim-clang-format'
Plug 'chriskempson/base16-vim'
Plug 'neomake/neomake'

call plug#end()

let base16colorspace=256 
colorscheme base16-default-dark

let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'cocstatus', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'filename': 'LightlineFilename',
      \   'cocstatus': 'coc#status'
      \ },
      \ }
autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()
" Quick-save
nmap <leader>w :w<CR>

let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0
let g:rust_clip_command = 'xclip -selection clipboard'

let g:clang_format#auto_format = 1
let g:clang_format#detect_style_file = 1

let g:ale_linters = { 'cpp': ['clangd'] }
let g:ale_fixers = { 'cpp': [ 'clang-format' ] }
" Permanent undo
set undodir=~/.vimdid
set undofile

set wildignore=.hg,.svn,*~,*.png,*.jpg,*.gif,*.settings,Thumbs.db,*.min.js,*.swp,publish/*,intermediate/*,*.o,*.hi,Zend,vendor

" Proper search
set incsearch
set ignorecase
set smartcase
set gdefault



" Jump to start and end of line using the home row keys
map H ^
map L $
" No arrow keys --- force yourself to use the home row
nnoremap <up> r
nnoremap <down> <nop>
inoremap <up> r
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
nnoremap <left> :bp<CR>
nnoremap <right> :bn<CR>

"
nnoremap <F1> :CocAction<CR><CR>

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the ouymbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)
set relativenumber " Relative line numbers
set number " Also show current absolute line

" <leader><leader> toggles between buffers
nnoremap <leader><leader> <c-^>

function! s:gitModified()
    let files = systemlist('git ls-files -m 2>/dev/null')
    return map(files, "{'line': v:val, 'path': v:val}")
endfunction
function! s:gitUntracked()
    let files = systemlist('git ls-files -o --exclude-standard 2>/dev/null')
    return map(files, "{'line': v:val, 'path': v:val}")
endfunction

let g:startify_session_persistence = 1
let g:startify_lists = [
        \ { 'type': 'sessions',  'header': ['   Sessions']       },
        \ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
        \ { 'type': function('s:gitModified'),  'header': ['   git modified']},
        \ { 'type': function('s:gitUntracked'), 'header': ['   git untracked']},
        \ { 'type': 'commands',  'header': ['   Commands']       },
        \ ]
