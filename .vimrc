set autochdir

set nocompatible
"" Better completion
set wildmenu
"" Allow cursor keys in insert mode.?
set esckeys
"" Highlight search
set hlsearch
"" Skip intro
set shortmess=atI
""Window bar filename
set title
""
set mouse=a
set ruler
""scroll before boundary.
set scrolloff=3
set tabstop=2
set expandtab
set clipboard=unnamedplus

set encoding=UTF-8
set noshowmode
set noswapfile

let mapleader = ","
""Save file as root
noremap <Leader>W :w !sudo tee % > /dev/null

noremap <Leader>w :update<CR>
noremap <Leader>t :tabnext<CR>
noremap <F5> :NERDTreeToggle<CR>

"Comment word
let @m='yiwve,cc'
"Replace word under cursor
":nnoremap <Leader>s :%s/\<<C-r><C-w>\>/
set laststatus=2

syntax on

color desert
set cursorline
hi CursorLine term=bold cterm=bold 

set number

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"----------------------------------------------------------------------------
call plug#begin('~/.vim/plugged')

"----------------------------------------------------------------------------
Plug 'Valloric/YouCompleteMe', { 'do': 'python install.py --rust-completer'}

let g:ycm_confirm_extra_conf = 0
let g:ycm_global_ycm_extra_conf = '~/.vim/ycm_extra_conf.py'
"----------------------------------------------------------------------------
Plug 'rhysd/vim-clang-format'
autocmd FileType c,cpp ClangFormatAutoEnable
"----------------------------------------------------------------------------
Plug 'itchyny/lightline.vim'
"----------------------------------------------------------------------------
Plug 'scrooloose/nerdtree'
"----------------------------------------------------------------------------
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }
"----------------------------------------------------------------------------

Plug 'rust-lang/rust.vim'
let g:rustfmt_autosave = 1
Plug 'scrooloose/syntastic'

"----------------------------------------------------------------------------
Plug 'altercation/vim-colors-solarized'
"----------------------------------------------------------------------------
call plug#end()

filetype plugin on



set path+=./**
set path+=/usr/include/c++/v1

