export LC_ALL=en_US.UTF-8


autoload -U colors && colors

if [[ ! -e ~/.antigen-repo ]]; then
    git clone https://github.com/zsh-users/antigen.git ~/.antigen-repo
fi
[[ -s /home/udo/.autojump/etc/profile.d/autojump.sh ]] && source /home/udo/.autojump/etc/profile.d/autojump.sh

autoload -U compinit && compinit -u

source ~/.antigen-repo/antigen.zsh
setopt promptsubst
antigen bundle git
antigen bundle colorize
# requires pkgfile package
antigen bundle command-not-found
antigen bundle hchbaw/auto-fu.zsh
antigen bundle Tarrasch/zsh-colors
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle jdavis/zsh-files
antigen bundle wting/autojump
antigen theme wezm

antigen apply

# Why is this not loaded automatically?
source ~/.antigen/bundles/jdavis/zsh-files/lib/git.zsh

bindkey -v

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Initialize completion
autoload -U compinit
zstyle ':completion:*' matcher-list '' \
  'm:{a-z\-}={A-Z\_}' \
  'l:[^[:alpha:]]||[[:alpha:]]=** l:|=* m:{a-z\-}={A-Z\_}' \
  'l:|?=** m:{a-z\-}={A-Z\_}'

compinit -D
# Nicer history
export HISTSIZE=100000
export HISTFILE="$HOME/.history"
export SAVEHIST=$HISTSIZE

export VISUAL="nvim"
export EDITOR="nvim"

#fzf
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# Disable Software Flow Controll for ctrlWS in vim
stty -ixon

alias o='xdg-open'
alias ls='exa'
alias ll='exa -l'
alias la='exa -a'
alias lla='exa -al'
alias ..='cd ..'
alias dotfiles='git --git-dir=$HOME/.mydotfiles/ --work-tree=$HOME'
alias diff='diff --color=auto'

export PATH=/home/udo/.cargo/bin:$PATH


export FZF_DEFAULT_COMMAND="rg --files --glob '!*/{.git,node_modules}/**'"
export FZF_CTRL_T_COMMAND="rg --files --glob '!*/{.git,node_modules}/**'"
export FZF_ALT_C_COMMAND="fd --type d --exclude node_modules --exclude .git"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty2 ]]; then
  exec sway
fi

#if [ "$TMUX" = "" ]; then tmux new-session -A -s default; fi
